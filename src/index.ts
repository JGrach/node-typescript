import dotenv from 'dotenv';
import express from 'express';
import { logger } from 'src/services/logger';

const { NODE_ENV = 'dev', NODE_CTX, APP_DOMAIN, APP_PROTOCOL, EXPRESS_PORT } = process.env;

if (NODE_ENV === 'dev' && NODE_CTX === 'dotenv') dotenv.config();

const app = express();

app.listen(EXPRESS_PORT, err => {
    if (err) logger.error(err);
    logger.info(`App started on ${APP_PROTOCOL}://${APP_DOMAIN}:${EXPRESS_PORT}`);
});
